#
# Google Hash Code 2020 Practice
# "More pizza"
# Team Tartaruga (Raphael Pithan, Carla Ourofino)
# 

import sys
from copy import copy

with open(sys.argv[1]) as f:
    [target, num_types] = [int(x) for x in f.readline().strip().split(' ')]
    slices = [int(x) for x in f.readline().strip().split(' ')]

def eager_order(used, sum, stop):
    for i in range(stop-1, -1, -1):
        if not used[i]:
            s = slices[i]
            if sum + s <= target:
                sum += s
                used[i] = True
                if sum == target:
                    break
    return { 'used': used, 'sum': sum }

def replace(order, index):
    replace_value = slices[index]
    used = copy(order['used'])
    sum = order['sum']
    used[index] = False
    sum -= replace_value
    new_order = eager_order(used, sum, index)
    return new_order

order = eager_order([ False ] * num_types, 0, num_types)
for i in reversed(range(num_types)):
    if order['used'][i] == True:
        new_order = replace(order, i)
        if new_order['sum'] >= order['sum']:
            order = new_order
        if order['sum'] == target:
            break

indices = []
for i in range(num_types):
    if order['used'][i]:
        indices.append(str(i))
print(str(len(indices)))
print(' '.join(indices))