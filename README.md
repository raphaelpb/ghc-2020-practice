# ghc-2020-practice

[Google Hash Code 2020](https://codingcompetitions.withgoogle.com/hashcode) - Practice round program  
"More pizza"

    Team Tartaruga

Score attained
----
A – example............................16  
B – small.............................100  
C – medium..........................4,500  
D – quite big...............1,000,000,000  
E – also big..................505,000,000  
**Total score**.............1,505,004,616  

Maximum score for all the test cases! =)

Program description
----

+ Python 3.5
+ No external libraries
+ Fast (better than O(n²))
+ Small (42 lines of code w/o comments)
+ Deterministic (not based on randomness)
+ 100% score for all the test cases

How does it work?

##### Start with the basic greedy algorithm:
For each number (pizza), highest to lowest (slice count), add it to the selection (order) if it doesn't go over the target value.

##### Break down the selected numbers into smaller sums:
For each selected number in the greedy solution, highest to lowest, remove it from the selection and try to replace it with a sum of lower numbers. Do it by applying the greedy algorithm to the available lower numbers. If the new solution performs equal or better than before, replace the current solution with it and move on to trying to replace the following lower number.

That's all.

##### Disclaimer
Despite having attained the best possible score for the proposed test cases, I have no guarantees that this algorithm is able to find the optimum solution in all cases, and I suspect it doesn't. If you can prove whether it does or doesn't, I'd be happy to hear about it.

Thank you.